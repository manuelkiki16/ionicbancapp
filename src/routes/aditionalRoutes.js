import React from "react";
import { Redirect, Route } from 'react-router-dom';
import {IonRouterOutlet} from "@ionic/react";


export default (props)=>{

    React.useEffect(()=>{
       console.log("Mathinh");
    },[]);

    return <>
        {
            props.subroutes.map(ele=>
                <Route key={ele.nameroute} exact={ele.exact} path={ele.path} render={props=>
                    <ele.component {...props}   routename={ele.nameroute} subroutes={ele.subroutes}  />
                  } >
                    
                  </Route>
                )
        }
    </>

}