import React from "react";
import {sesionStorageId} from "../utils/contants";
import LoginController from "../controllers/LoginController";
import RegistroController from "../controllers/RegistroController";
import InitViewController from "../controllers/InitController";
import AddAccountController from "../controllers/AddAccountController";
import GenerateQrCodeController  from "../controllers/QrGenerateController";
import SelfTransactionController  from "../controllers/SelfTransaccionController";
import TransactionHistoryController from "../controllers/TransactionHistoryController";

/* import Tabs from "" */
import TabController from "../controllers/TabController";
import AdditionalRoutes from "./aditionalRoutes";

import SuccessView from "../views/successRegister";
import TransactionSuccess from "../views/TransactionSuccess";


const routes = [
    {
        component:InitViewController,
        path:"/",
        nameroute:"init",
        auth:false,
        exact:true,
        subroutes:[]
    },
    {
        component:SuccessView,
        path:"/successView",
        nameroute:"successView",
        auth:false,
        exact:true,
        subroutes:[]
    },
    {
        component:LoginController,
        path:"/Login",
        nameroute:"Login",
        auth:false,
        exact:true,
        subroutes:[]
    },
    {
        component:RegistroController,
        path:"/registro",
        nameroute:"Registro",
        auth:false,
        exact:true,
        subroutes:[]
    },
    {
        component:TabController,
        path:"/tabview",
        nameroute:"Loggedroutes",
        auth:true,
        exact:false,
        subroutes:[]
    },
    {
        component:AdditionalRoutes,
        path:"/aditionalroutes",
        auth:true,
        nameroute:"AditionalViews",
       
        subroutes:[{
            component:AddAccountController,
            path:"/aditionalroutes/addaccount/:typeReg",
            nameroute:"AddAccount",
            exact:true,
            subroutes:[{
    
            }]
            
        },
        {
            component:TransactionSuccess,
            path:"/aditionalroutes/transactionSuccess",
            nameroute:"TransactionSuccess",
            exact:true,
            subroutes:[{
    
            }]
            
        },
        
        {
            component:GenerateQrCodeController,
            path:"/aditionalroutes/generateQrCode",
            nameroute:"GenerateQrCode",
          
            subroutes:[{
    
            }]

        },
        {
            component:SelfTransactionController,
            path:"/aditionalroutes/selfTransaccion/:typeTransaccion",
            nameroute:"SelfTransactions",
          
            subroutes:[{
    
            }]

        },
        {
            component:TransactionHistoryController,
            path:"/aditionalroutes/transactionHistory/:AccountInId",
            nameroute:"transactionHistory",
          
            subroutes:[{
    
            }]

        }
    ]
    },
];



export default  routes;