import React,{useEffect,useState} from "react";
import InitView from "../views/InitView";
import Service from "../service/AxiosInstance";
import {sesionStorageId} from "../utils/contants";
import {useHistory} from "react-router-dom";
import {useDispatch,useSelector} from "react-redux";
import {AlertAction,SetEntities,SetSesionStatus} from "../Redux/actions";


export default function InitController(props){

    let history = useHistory();
    let dispatch = useDispatch();
    let sesionStatus = useSelector(state=>state.SesionStatus );
    

    useEffect(()=>{
        console.log("In routes");

        let sesionToken = localStorage.getItem(sesionStorageId);

        if( sesionToken ){

            LoadEntity().then( res=>{
                
                if (res == false){
                        
                    dispatch(AlertAction({visible:true,msg:"Error revise su conexion a internet"}));

                }else{
                    
                    dispatch(SetEntities(res.value));
                    dispatch(SetSesionStatus(true));

                   
                    history.push(`/tabview/myaccount`);
                
                    //history.replace("/tabview");
                }
                
            } ).catch(e=>{
                console.log(e);
            });


        }else{

            LoadEntity().then(  res=>{
                if (res == false){
                        
                    dispatch(AlertAction({visible:true,msg:"Error revise su conexion a internet"}));

                }else{
                    dispatch(SetEntities(res.value));
                    history.push("/Login");
                }
            });
          

        }


    },[sesionStatus]);

    function LoadEntity(){

        return new Promise( resolve=>{

            Service.get("/entities").then(result=>{
                    resolve(result.data);
            }).catch(err=>{
                console.log(err);
                resolve(false);
            });


        });

    }




    return (
        <InitView 
        />
    )


}