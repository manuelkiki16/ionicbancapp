import React,{useState,useEffect} from "react";
import TransaccionHistoryView from "../views/TransactionHistory";
import {useHistory,useParams} from "react-router-dom";
import Service from "../service/AxiosInstance";
import AlertComponent from "../components/AlertComponent";
import {LoadingAction} from "../Redux/actions";
import {useSelector} from "react-redux";



export default function TransactionHistoryController(props){


    let [typeView,setTypeView] = useState("SENDED");
    let {AccountInId} = useParams();
    let [transactionPainted,setTransactionPainted] = useState([]);
    let [alertConfig,setAlertConfig] = useState({visible:false,message:""});
    let AccountSelected = useSelector(state=>state.MyAccountSelected);
    let [transactions,setTransactions] = useState({
        send:[],
        received:[]
    });


    useEffect(() => {

        Service.post("/transactions/transactionHistory",{accounInscriptionId:AccountInId})
        .then(result=>{
           
            if( typeView == 'SENDED'){
                console.log(result.data);
                setTransactionPainted(result.data.value.transactionSendCopy  );
                setTransactions({
                    send:result.data.value.transactionSendCopy,
                    received:result.data.value.transactionReceivedCopy

                });
            }
    
    
        }).catch(error=>{
                console.log(error);
                if(error.response ){
                    
                        setAlertConfig({visible:true,message:error.response.data.msg});
                }else{
                    setAlertConfig({visible:true,message:error.message});
                }
        }).finally(()=>{
    
        });

        
       
    }, []);

    useState(()=>{
        typeView == 'SENDED'? setTransactionPainted(transactions.send ):setTransactionPainted(transactions.received );
    },[typeView]);



    


return(
   <>
         <AlertComponent
        {...alertConfig}
        closeCallback={()=>setAlertConfig({...alertConfig,visible:false})}
        callback={()=>{}}
        /> 
        <TransaccionHistoryView 
        Account={AccountSelected}
        typeView={typeView}
        setTypeView={setTypeView}
        transactionPainted={transactionPainted}
        />
   </>
)

}