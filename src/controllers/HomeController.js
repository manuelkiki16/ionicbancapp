import React,{useEffect,useState} from "react";
import HomeView from "../views/HomeView";
import Service from "../service/AxiosInstance";
import {useDispatch,useSelector} from "react-redux";
import {SetSelfAccounts,AlertAction,SetMyAccountSelected} from "../Redux/actions";
import { useHistory } from "react-router-dom";
import AccountActionComponent from "../components/AccountActionComponent";


export default function HomeController(props){

    let dispatch = useDispatch();
    let history = useHistory();
    let [accountId,setAccountId] = useState(null);
   
    let selfAccounts = useSelector( state => state.MyacountList );
    let [Sheetvisible,setSheetVisible] = useState(false);


    useEffect(()=>{
        GetAccountList();


    },[]);


    function GetAccountList(){
        let params = {
            accountInscriptionType:"SELF"
        }
        Service.post("/accounts/listAccount",params).then(result=>{

            dispatch(SetSelfAccounts(result.data.value));


        }).catch(error=>{

        });
    }

    function OpensheetOptions(index){
        setAccountId([...selfAccounts][index].accountInscriptionId);
        dispatch(SetMyAccountSelected([...selfAccounts][index]));
        setSheetVisible(true);

    }
    function CloseSheet(){
        setSheetVisible(false);
        dispatch(SetMyAccountSelected({}));
    }
    function CancelSheet(){
        setSheetVisible(false);
    }
    function SelectedSheetOptions(typeOp){

        switch (typeOp) {

            case "QR-G":

                history.push("/aditionalroutes/generateQrCode");

                break;

            case "T-P":

                history.push("/aditionalroutes/selfTransaccion/SELF_ACCOUNT");

                break;
            case "T-T":

                history.push("/aditionalroutes/selfTransaccion/OTHER_ACCOUNT");
    
                break; 
            case "H-T":
                history.push("/aditionalroutes/transactionHistory/"+accountId);  
                break; 
            case "T-C-G":
                
                break;       

            default:
                break;
        }

    }



    return (
        <>
        <HomeView
        accountList={selfAccounts}
        onOpenSheetOp={OpensheetOptions}
        createNewAccount={()=>history.push("/aditionalroutes/addaccount/SELF")}
        joinOtherAccount={()=>history.push("/aditionalroutes/addaccount/OTHER")}
        />
        <AccountActionComponent 
        visible={Sheetvisible}
        onSelectOption={SelectedSheetOptions}
        onCloseOpe={CloseSheet}
        CancelSheet={CancelSheet}
        />
        </>

    )
}