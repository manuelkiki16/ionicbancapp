import React,{useEffect,useState} from "react";
import AddAccountView from "../views/AddAccount";
import Service from "../service/AxiosInstance";
import {useDispatch,useSelector} from "react-redux";
import {SetSelfAccounts,LoadingAction,AlertAction} from "../Redux/actions";
import { useHistory,useParams } from "react-router-dom";
import AlertComponent from "../components/AlertComponent";



export default function AddAccountController(props){

     
    let dispatch = useDispatch();
    let history = useHistory();
    let {typeReg} = useParams();


    
    let [alertConfig,setAlertConfig] = useState({visible:false,message:"",title:"",callback:()=>{}});
    let [accountNumber,setAccountNumber] = useState("");
    let [accountType,setAccountType] = useState("");
    let [accountNick,setAccountNick] = useState("");
    let [accountDocId,setAccountDocId] = useState("");
    let [accountEntitiId,setAccountEntitiId] = useState("");

    let [validatorAc,setValidatorAc] = useState({
        accountNumber:{msg:"",vs:false},
        accountType:{msg:"",vs:false},
        accountNick:{msg:"",vs:false},
        accountDocId:{msg:"",vs:false},
        accountEntitiId:{msg:"",vs:false},
    });


    function AccountValidator(){
        let state = true;
        let validatorCopy = {...validatorAc};

        if(accountNumber){

            if( Boolean( Number(accountNumber) ) ){

                if( accountNumber.length != 11 ){

                    validatorCopy.accountNumber.vs = true;
                    validatorCopy.accountNumber.msg = "el numero debe tener 11 digitos";
                    state=false;

                }else{

                    validatorCopy.accountNumber.vs = false;
                    validatorCopy.accountNumber.msg = "";
                }
              
            }else{
                
                validatorCopy.accountNumber.vs = true;
                validatorCopy.accountNumber.msg = "Solo numeros requeridos";
                state=false;

            }
        }else{
            state=false;
            validatorCopy.accountNumber.vs = true;
            validatorCopy.accountNumber.msg = "Campo requerido";
        }
        if( accountType == '' ){

            state=false;
            validatorCopy.accountType.vs = true;
            validatorCopy.accountType.msg = "Seleccione un tipo de cuenta";

        }else{

            validatorCopy.accountType.vs = false;
            validatorCopy.accountType.msg = ""; 

        }
        if( accountNick == '' ){

            state=false;
            validatorCopy.accountNick.vs = true;
            validatorCopy.accountNick.msg = "Campo requerido";

        }else{

            validatorCopy.accountNick.vs = false;
            validatorCopy.accountNick.msg = ""; 

        }
        

        if( accountDocId == '' ){
            state=false;
            validatorCopy.accountDocId.vs = true;
            validatorCopy.accountDocId.msg = "Campo requerido";
        }else{
           
            if( accountDocId.length == 12  ){

                if( Boolean( Number( accountDocId ) ) ){

                    validatorCopy.accountDocId.vs = false;
                    validatorCopy.accountDocId.msg = ""; 
                }else{
                    validatorCopy.accountDocId.vs = true;
                    validatorCopy.accountDocId.msg = "Solo numeros requeridos";
                    state=false;
                }

            }else{

                state=false;
                validatorCopy.accountDocId.vs = true;
                validatorCopy.accountDocId.msg = "El documento debe tener 12 caracteres";

            }
          
        }
        if( accountEntitiId == '' ){
            state=false;
            validatorCopy.accountEntitiId.vs = true;
            validatorCopy.accountEntitiId.msg = "Seleccione una entidad";
        }else{
            validatorCopy.accountEntitiId.vs = false;
            validatorCopy.accountEntitiId.msg = "";
        }

        setValidatorAc(validatorCopy);



        return state;

    }

    useEffect(() => {
     
    }, []);

    function OnRegisterAccount(){
        
        if( !AccountValidator() ){
            return ;
        }


              let params = {
                accountNumber:accountNumber,
                accountType:accountType,
                accountInscriptionNickName:accountNick,
                accountInscriptionDocId:accountDocId,
                accountInscriptionType:typeReg,
                EntityId:accountEntitiId
                
              };
              dispatch(LoadingAction({visible:true,msg: typeReg =="Registrando cuenta"}));  

       
                Service.post("/accounts"+(typeReg == 'SELF'?'/createAccount':"/joinAccount"),params).then(result=>{
                    
                   
                    setAlertConfig({visible:true,message:result.data.msg,callback:()=>history.goBack()});
                  
                }).catch(error=>{
                    if( error.response?.data.status == 400 ){
                      setAlertConfig({visible:true,message:error.response.data.msg});
                    }else{
                      setAlertConfig({visible:true,message:"Error de conexion al servidor"});
                    }
                }).finally( ()=>{
                  dispatch(LoadingAction({visible:false,msg:""}));  
                } );
           


        



    }



    return (
       <>
       <AlertComponent {...alertConfig} closeCallback={()=>setAlertConfig({...alertConfig,visible:false})}   />
        <AddAccountView 
        typeReg={typeReg}
        setAccountNumber={setAccountNumber}
        setAccountType={setAccountType}
        setAccountNick={setAccountNick}
        setAccountDocId={setAccountDocId}
        setAccountEntitiId={setAccountEntitiId}
        validdationsFIelds={validatorAc}
        OnRegisterAccount={OnRegisterAccount}


        
        />
       </>
    )


}