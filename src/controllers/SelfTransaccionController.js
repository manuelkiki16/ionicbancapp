import React,{useState,useEffect} from "react";
import SelfTransaccionView from "../views/SelfTransactions";
import ModalAccountList from "../components/ModalAccountList";
import {SetOtherAccountSelected,LoadingAction,SetTransactionDetail} from "../Redux/actions";
import {useDispatch,useSelector} from "react-redux";
import Service from "../service/AxiosInstance";
import {useParams,useHistory} from "react-router-dom";
import AlertComponent from "../components/AlertComponent";


export default function SelfTransaccionController(props){

    let [modalVisible,setModalVisible] = useState(false);
    let dispatch = useDispatch();
    let history = useHistory();
    let {typeTransaccion} = useParams();
    let [accoutList,setAccountList] = useState([]);
    let [totalAmount,setTotalAmount] = useState( 0);
    let [transactionDescription,setTransactionDescription] = useState("");
    let otherAccountList = useSelector(state=>state.OtherAccountLIst);
    let myacountList = useSelector(state=>state.MyacountList);
    let OtherAccountSelected = useSelector(state=>state.OtherAccountSelected);
    let MyAccountSelected = useSelector(state=>state.MyAccountSelected);
    let [AlertConfig,setAlertConfig] = useState({visible:false,message:"",title:null});
 
   


    function SelectAccount (ele){
        
        setModalVisible(false);
        dispatch(SetOtherAccountSelected( ele ));
        
       

    }

    function MakeTransaction(){

        if( Boolean( Number(totalAmount) ) ){

            if( MyAccountSelected.Account.accountBalance < Number(totalAmount)   ){

                    setAlertConfig({visible:true,message:"Tu cuenta no tiene saldo suficiente"});
                    return ;
            }
            let params ={
                transacionAmount:totalAmount,
                accounIntOriginId:MyAccountSelected.accountInscriptionId,
                accountInDestinationId:OtherAccountSelected.accountInscriptionId,
                transactionDescription:transactionDescription
            };
            dispatch(LoadingAction({visible:true,msg:"Realizando transferencia"}));
            Service.post("/transactions/makeTransaccion",params).then(result=>{

                dispatch(SetTransactionDetail(result.data.value));
                history.replace("/aditionalroutes/transactionSuccess");
               

            }).catch(error=>{

                if( error.response?.data.status == 400 ){

                    setAlertConfig({visible:true,message:error.response?.data.msg});

                }else{
                        setAlertConfig({visible:true,message:error.message});


                }

            }).finally(()=>{
                dispatch(LoadingAction({visible:false,msg:""}));
            });



            
        }else{
            setAlertConfig({visible:true,message:"El monto debe ser numerico"});
        }

    }

    useEffect(() => {

        if(typeTransaccion == 'SELF_ACCOUNT'){
          
            let ListAccounts = myacountList.filter( ele=>ele.accountInscriptionId !=  MyAccountSelected.accountInscriptionId);
            
            setAccountList(ListAccounts);

        }else{

            dispatch( LoadingAction({visible:true,msg:"Cargando listado"}) );

            Service.post("/accounts/listAccount",{accountInscriptionType:"OTHER"}).then(result=>{
                setAccountList(result.data.value);
    
    
            }).catch(error=>{
                if( error.response?.data.status ){
                    setAlertConfig({visible:true,message:error.response?.data.msg});
                }else{
                    setAlertConfig({visible:true,message:error.message});
                }
    
    
            }).finally(()=>{
                dispatch( LoadingAction({visible:false,msg:""}) );
            });

        }

        

        
    }, [otherAccountList,myacountList])

    useEffect(() => {

       
       
    }, [])



    return (
        <>
        <AlertComponent
         {...AlertConfig}
        callback={()=>{}}
        closeCallback={()=>setAlertConfig({...AlertConfig,visible:false} )}
        />
        <ModalAccountList
            isVisible={modalVisible}
            changeVisibility={setModalVisible}
            accountList={accoutList}
            selectAccount={SelectAccount}
        />
        <SelfTransaccionView 
            MakeTransaction={MakeTransaction}
            setTotalAmount={setTotalAmount}
            setTransactionDescription={setTransactionDescription}
            openSelectAccount={setModalVisible}
            MyAccountSelected={MyAccountSelected}
            OtherAccountSelected={OtherAccountSelected}
        />
        </>
    )

}