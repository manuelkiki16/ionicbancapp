import React,{useState,useEffect} from "react";
import  Registro from "../views/Registro";
import Service from "../service/AxiosInstance";
import {sesionStorageId} from "../utils/contants";
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {AlertAction,LoadingAction,SetEntities} from "../Redux/actions";
import AlertComponent from "../components/AlertComponent";



export default function RegistroController(props){


    let history = useHistory();
    let dispatch = useDispatch();
    let [alertConfig,setAlertConfig] = useState({visible:false,message:"",title:""});
    let [userName,setUserName] = useState("");
    let [userEmail,setUserEmail] = useState("");
    let [userPassword,setUserPassword] = useState("");
    let [userRepeatPassword,setUserRepeatPassword] = useState("");
    let [validdationsFIelds,setValidationFIelds] = useState({
        userName:{
            vs:false,
            msg:""
        },
        userEmail:{
            vs:false,
            msg:""
        },
       
        userPassword:{
            vs:false,
            msg:""
        },
        userRepeatPassword:{
            vs:false,
            msg:""
        }

    });


    function ValidateInpus(){
        let state = true;
        let validatorCopy = {...validdationsFIelds};

        if( !userName ){
            state = false;
            validatorCopy.userName.vs = true;
            validatorCopy.userName.msg ="Campo requerido";
                
        }else{
            
            validatorCopy.userName.vs = false;
            validatorCopy.userName.msg ="";

        }
        if( !userEmail ){
            state = false;
            validatorCopy.userEmail.vs = true;
            validatorCopy.userEmail.msg ="Campo requerido";
                
        }else{
            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if( emailPattern.test(userEmail) ){

                validatorCopy.userEmail.vs = false;

            }else{
                state = false;
                validatorCopy.userEmail.vs = true;
                validatorCopy.userEmail.msg = 'Correo no valido';
            }

          
        }
        if( !userPassword ){
            state = false;
            validatorCopy.userPassword.vs = true;
            validatorCopy.userPassword.msg ="Campo requerido";
                
        }else{
            console.log( userPassword.length);
            if( userPassword.length >7 ){
                validatorCopy.userPassword.vs = false;
            }else{
            
                state = false;
                validatorCopy.userPassword.vs = true;
                validatorCopy.userPassword.msg ="Minimo 8 caracteres";
            }
            
          
        }
        if( !userRepeatPassword ){
            state = false;
            validatorCopy.userRepeatPassword.vs = true;
            validatorCopy.userRepeatPassword.msg ="Campo requerido";
                
        }else{

            if( userPassword != userRepeatPassword  ){

                state = false;
                validatorCopy.userRepeatPassword.vs = true;
                validatorCopy.userRepeatPassword.msg ="Las contraseñas no coinciden";
            }else{
                validatorCopy.userRepeatPassword.vs = false;
            }
            
         
        }
        setValidationFIelds(validatorCopy);


        return state;

    }

    function RegisterAction(){
        
        if( ValidateInpus() ){

            dispatch(LoadingAction({visible:true,msg:"Registrando datos"}));

            let params = {
                userFirstName:userName,
                userLastName:"",
                userEmail:userEmail,
                userPassword:userPassword
            };

            Service.post("createUser",params).then(result=>{

                dispatch(LoadingAction({visible:false,msg:""}));

               history.push("/successView");

            }).catch(error=>{
                setAlertConfig({
                    visible:true,
                    message:"Error al tratar de finalizar el registro"
                });
              
            }).finally(()=>{
                setAlertConfig({
                    visible:false,
                    message:""
                });
              
            });




        }

    }




    return (
      <>
        <AlertComponent
        {...alertConfig}
        closeCallback={()=>setAlertConfig({...alertConfig,visible:false})}
        />
        <Registro
        goToLogin={()=>history.push("/Login")}
        setUserName={setUserName}
        setUserEmail={setUserEmail}
        setUserPassword={setUserPassword}
        setUserRepeatPassword={setUserRepeatPassword}
        RegisterAction={RegisterAction}
        validdationsFIelds={validdationsFIelds}

        />
      </>
    )

}