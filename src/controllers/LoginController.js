import React,{useEffect,useState} from "react";
import LoginView from "../views/Login";
import Service from "../service/AxiosInstance";
import {sesionStorageId} from "../utils/contants";
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {AlertAction,LoadingAction,SetEntities,SetSesionStatus} from "../Redux/actions";
import AlertComponent from "../components/AlertComponent";


export default function LoginController(props){

    let dispatch = useDispatch();
    let history = useHistory();
    let [alertConfig,setAlertConfig] = useState({visible:false,message:"",title:""});
    let [userEmail,setUserEmail] = useState("");
    let [userPassword,setUserPassword] = useState("");
    let [validdationsFIelds,setValidationFIelds] = useState({
     
        userEmail:{
            vs:false,
            msg:""
        },
       
        userPassword:{
            vs:false,
            msg:""
        }

    });

   
    useEffect(()=>{
        console.log(props.history);
    },[]);

    function goToRegister(){
        props.history.push("registro");
    }
    function GoToTabs(){
        props.history.push("tabview");
    }



    function ValidateInpus(){
        let state = true;
        let validatorCopy = {...validdationsFIelds};

        
        if( !userEmail ){
            state = false;
            validatorCopy.userEmail.vs = true;
            validatorCopy.userEmail.msg ="Campo requerido";
                
        }else{
            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if( emailPattern.test(userEmail) ){

                validatorCopy.userEmail.vs = false;

            }else{
                state = false;
                validatorCopy.userEmail.vs = true;
                validatorCopy.userEmail.msg = 'Correo no valido';
            }

          
        }
        if( !userPassword ){
            state = false;
            validatorCopy.userPassword.vs = true;
            validatorCopy.userPassword.msg ="Campo requerido";
                
        }else{
            
            validatorCopy.userPassword.vs = false;
        }
       
        setValidationFIelds(validatorCopy);


        return state;

    }



    function OnLoginAction(){

        if( ValidateInpus() ){
            dispatch(LoadingAction({visible:true,msg:"validando uusuario"}));
            let params ={
                userEmail:userEmail,
                userPassword:userPassword
            };
            Service.post("/login",params).then(result=>{
                dispatch(SetSesionStatus(true));
                localStorage.setItem(sesionStorageId,result.data.value.tokenUser);
               
                history.push("/");
               

            }).catch(error=>{
            console.log(error,error.response);
             if( error.response?.data.status == 400){
                setAlertConfig({visible:true,message:error.response.data.msg});
              

             }else{
                setAlertConfig({visible:true,message:error.message});
               
             }


            }).finally(()=>{
                dispatch(LoadingAction({visible:false,msg:"validando uusuario"}));
            });


        }else{

        }

    }

return (
<>
<LoginView 
    goToRegister={goToRegister}
    GoToHome={OnLoginAction}
    validdationsFIelds={validdationsFIelds}
    setUserEmail={setUserEmail}
    setUserPass={setUserPassword}
    />
    <AlertComponent {...alertConfig} closeCallback={()=>setAlertConfig({...alertConfig,visible:false})}   />
)
</>
)
}