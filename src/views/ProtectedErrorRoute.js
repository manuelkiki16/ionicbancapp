import React from "react";
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonTitle,
  IonToolbar,
  IonButton,
  IonGrid,
  IonRow,
  IonCol,
  IonText,
  IonFab,
  IonFabButton,
  IonFabList,
  IonChip,
  IonButtons,
  IonPage,
  IonBackButton
  

  } from '@ionic/react';
import { book, build, colorFill, grid,logoVimeo,settings,personCircle,search,options } from 'ionicons/icons';


export default function ProtectedErrorRoute(){


    return (<IonPage>

        <IonContent
        >
            <div className='row-text-center' >

                <IonText className='congratulation-title' >No login</IonText>
                <IonText className='congratulation-description' >Se requiere inicio de sesion</IonText>

                <IonButton color="primary"  >Ir al login</IonButton>

            </div>

        </IonContent>

    </IonPage>)

}