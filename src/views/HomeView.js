import React, { useEffect } from "react";
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonIcon,

  IonTitle,
  IonToolbar,
  IonButton,
  IonGrid,
  IonRow,
  IonCol,
  IonText,

  IonButtons,
  

  } from '@ionic/react';
import { book, build, colorFill, grid,logoVimeo,settings,personCircle,search,options,addCircle } from 'ionicons/icons';
import AccountCardComponent from "../components/AccountCard";

export default function HomeView(props){





    return (
      <React.Fragment>

      <IonContent>
       <IonHeader>
         <IonToolbar>
           <IonTitle>Mis cuentas</IonTitle>
           <IonButtons slot="secondary">
       
     
    </IonButtons>
           
         </IonToolbar>
       </IonHeader>
        <IonGrid>
            <IonRow>
              <IonCol  size='6'  >
                  <IonCard onClick={props.createNewAccount} className='card-tab' >

                      <IonIcon className='ic-card' icon={addCircle}  />
                      <IonText className='text-white'  >Cuenta propia</IonText>
                  
                  </IonCard>
              </IonCol>
              <IonCol  size='6'  >
                  <IonCard onClick={props.joinOtherAccount} className='card-tab' >

                      <IonIcon className='ic-card' icon={addCircle}  />
                      <IonText className='text-white'  >Cuenta tercero</IonText>
                  
                  </IonCard>
              </IonCol>
              {props.accountList?.map( (ele,index)=>
                <AccountCardComponent 
                key={ele.accountInscriptionId.toString()}
                index={index}
                accountId={ele.Account.accountId}
                OnOpenSheet={props.onOpenSheetOp}
                typeAccount={ele.Account.accountType}
                accountNumber={ele.Account.accountNumber}
                accountBalance={ele.Account.accountBalance}
              
                />
                )}

            </IonRow>
        </IonGrid>

      
      </IonContent>
    </React.Fragment>
    )


}