import React,{useEffect} from "react";
import {IonPage,
    IonCard,
    IonCardHeader,
    IonCardContent,
    IonContent,
    IonRow,
    IonCol,
    IonHeader,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButtons,
    IonCardTitle,
    IonText,
    IonItem,
    IonLabel,
    IonInput,
    IonCardSubtitle,
    IonSelect,
    IonSelectOption,
    IonButton,
    IonTextarea} from "@ionic/react";
import QRCode from "qrcode.react";
import {useDispatch,useSelector} from "react-redux";
import {FormatValue} from "../utils/methods";



export default function(props){


 

    return (

    <IonPage>

            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/" />
                    </IonButtons>
                    <IonTitle>Portal Transaccional</IonTitle>
                </IonToolbar>

              </IonHeader>
              <IonContent>
              <IonCard>
                    <IonCardHeader>
                    <IonCardTitle>Cuenta de origen </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                    
                        <IonCardSubtitle><strong>Cuenta N°:</strong>  {props.MyAccountSelected?.Account.accountNumber}  </IonCardSubtitle>
                        <IonCardSubtitle><strong>Alias:</strong>  {props.MyAccountSelected?.accountInscriptionNickName}</IonCardSubtitle>
                        <IonCardSubtitle><strong>Disponible:</strong>  $ {FormatValue(props.MyAccountSelected?.Account.accountBalance)} COP</IonCardSubtitle>
                      
                    </IonCardContent>
                
                </IonCard>   
                <IonCard>
                    <IonCardHeader>
                    <IonCardTitle>Cuenta de destino </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                    
                        <IonCardSubtitle><strong>Cuenta N°:</strong>  {props.OtherAccountSelected?.Account?.accountNumber} </IonCardSubtitle>
                        <IonCardSubtitle><strong>Alias:</strong>  {props.OtherAccountSelected?.accountInscriptionNickName}</IonCardSubtitle>
                       
                        <IonButton  onClick={()=>props.openSelectAccount(true)} size="small" color='secondary' >Seleccionar cuenta</IonButton>
                    </IonCardContent>
                
                </IonCard> 
                <IonCard>
                <IonCardHeader>
                    <IonCardTitle className='text-center'  >Cantidad a transferir </IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                <IonItem>
                    <IonInput onIonChange={evt=>props.setTotalAmount(evt.target.value)} type="number" className='text-center' placeholder="eje:100000" ></IonInput>
                 </IonItem>
                 <IonItem>
                    <IonTextarea placeholder="Descripcion de la transferencia" maxlength={100} onIonChange={e => props.setTransactionDescription(e.detail.value)}></IonTextarea>
                </IonItem>
                </IonCardContent>
                
                </IonCard>  
                <IonButton onClick={props.MakeTransaction} expand="full" className='text-center' color='success' >Realizar la transaccion</IonButton>  

              </IonContent>



    </IonPage>
    )


}