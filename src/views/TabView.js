import React,{useEffect,useState} from "react";
import { IonTabs, IonTabBar,IonAlert, IonTabButton,IonRouterOutlet,IonIcon,IonTab,IonLabel, IonBadge } from '@ionic/react';
import { albums, personCircle, map, informationCircle,book } from 'ionicons/icons';
import HomeController from "../controllers/HomeController";
import { Redirect, Route,useHistory } from 'react-router-dom';

export default ()=>{




  useEffect(()=>{
   
/*     let unblock = history.block(tx => {
    
      let url = tx.location.pathname;
      if (window.confirm(`Are you sure you want to go to ${url}?`)) {
        // Unblock the navigation.
        unblock();
    
        // Retry the transition.
        tx.retry();
      }
    }); */

  },[]);

return (
  <IonTabs>

<IonTabBar slot="bottom">
  <IonTabButton href='/tabview/myaccount' tab="myaccount">
    <IonIcon icon={albums} />
    <IonLabel>Mis cuentas</IonLabel>
    <IonBadge>6</IonBadge>
  </IonTabButton>

  <IonTabButton tab="speakers">
    <IonIcon icon={book} />
    <IonLabel>Historial</IonLabel>
  </IonTabButton>

  <IonTabButton tab="map">
    <IonIcon icon={map} />
    <IonLabel>Mis datos</IonLabel>
  </IonTabButton>


</IonTabBar>
<IonRouterOutlet>
  
  <Route path="/tabview/myaccount" render={ props=><HomeController {...props}  />} />
  <Route path="/:tab(speakers)" render={ props=><HomeController {...props}  />} exact={true} />
  <Route path="/:tab(speakers)" render={ props=><HomeController {...props}  />} exact={true} />
 {/*  <Route exact path="/" render={() => <Redirect to="/tab1" />} /> */}
</IonRouterOutlet>
</IonTabs>
)

}