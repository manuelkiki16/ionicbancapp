import React from "react";
import { IonPage, IonHeader, IonToolbar, mIonContent,IonImg,IonItem,IonInput, IonRow, IonCol, IonLabel, IonButton, IonButtons, IonTitle, IonBackButton, IonContent, IonText, IonGrid } from "@ionic/react";
import CustomHeader from "../components/CustomToolbar";
import IcLogo from "../assets/images/ic_logo.svg";

export default function Registro(props) {

    return (

        <IonPage  >

            

        <IonContent className='padding-page-top' >
           <IonGrid>
               
            <IonCol>
                <IonCol size='12' >
                   
                </IonCol>
                <IonCol size='12' >
                      <div className='registro-container' >
                        <IonText  className='title-form' >Registrate para acceder a nuestros servicios</IonText>

                        <div className='form-container-reg' >
                            <IonItem>
                                <IonLabel color='primary' position="stacked">Nombre de usuario</IonLabel>
                                <IonInput onIonChange={evt=>props.setUserName(evt.target.value)} className='ion-input'  placeholder='nombre de usuario'  ></IonInput>
                               { props.validdationsFIelds.userName.vs == true? <label className='label-error' >{props.validdationsFIelds.userName.msg}</label>:<></>}
                            </IonItem>
                           
                            <IonItem>
                                <IonLabel color='primary' position="stacked">Correo</IonLabel>
                                <IonInput  onIonChange={evt=>props.setUserEmail(evt.target.value)}  className='ion-input' type="email" placeholder='example@domain.com'  ></IonInput>
                                { props.validdationsFIelds.userEmail.vs? <label className='label-error' >{props.validdationsFIelds.userEmail.msg}</label>:<></>}
                            </IonItem>
                            <IonItem>
                                <IonLabel color='primary' position="stacked">Contraseña</IonLabel>
                                <IonInput onIonChange={evt=>props.setUserPassword(evt.target.value)}   className='ion-input' type="password" placeholder='*******'  ></IonInput>
                                { props.validdationsFIelds.userPassword.vs? <label className='label-error' >{props.validdationsFIelds.userPassword.msg}</label>:<></>}
                            </IonItem>
                            <IonItem>
                                <IonLabel color='primary' position="stacked">Repita su contraseña</IonLabel>
                                <IonInput onIonChange={evt=>props.setUserRepeatPassword(evt.target.value)} className='ion-input' type="password" placeholder='*******'  ></IonInput>
                                { props.validdationsFIelds.userRepeatPassword.vs? <label className='label-error' >{props.validdationsFIelds.userRepeatPassword.msg}</label>:<></>}
                            </IonItem>
                                <IonButton  onClick={props.RegisterAction} expand="full" className='btn-rounded btn-primary' >
                                    <IonText className='white' >Registrarse</IonText>
                                </IonButton>
                        </div>
                      </div>
                    </IonCol>
                    <IonCol >
                    <div className='text-center m-t-2'  >
                                <IonText  className='text-color txt-small ' >¿Ya tienes una cuenta</IonText><IonText className='txt-small '  color="primary"> Inicia sesion</IonText> ?
                            </div>
                      
                    </IonCol>
            </IonCol>

           </IonGrid>
        </IonContent>
      </IonPage>

    )

}