import React from "react";
import {IonPage,IonHeader,IonContent,IonToolbar,IonButton,IonGrid,IonRow,IonCol,IonList,IonItem,IonLabel,IonInput,IonImg,IonText} from "@ionic/react";
import IcLogo from "../assets/images/ic_logo.svg";

export default function Login(props){


    return (
        <IonPage>

            <IonContent>

                <IonGrid>
                    <IonRow  >

                    <IonCol size='12' >
                      <div className='logo-header' >
                            <IonImg src={IcLogo} className='logo-app' />
                            <IonText  className='title-form' >Login app</IonText>
                      </div>
                    </IonCol>
                        <IonCol size='12' >

                            <div className='login-container' >

                                <IonItem>
                                    <IonLabel color='primary' position="stacked">correo electronico</IonLabel>
                                    <IonInput onIonChange={evt=>props.setUserEmail(evt.target.value)} className='ion-input' placeholder='example@domain.com'  ></IonInput>
                                    { props.validdationsFIelds.userEmail.vs == true? <label className='label-error' >{props.validdationsFIelds.userEmail.msg}</label>:<></>}
                                </IonItem>
                                <IonItem>
                                    <IonLabel color='primary' position="stacked">contraseña</IonLabel>
                                    <IonInput onIonChange={evt=>props.setUserPass(evt.target.value)} className='ion-input' type="password" placeholder='contraseña'  ></IonInput>
                                    { props.validdationsFIelds.userPassword.vs == true? <label className='label-error' >{props.validdationsFIelds.userPassword.msg}</label>:<></>}
                                </IonItem>
                                <IonButton onClick={()=>props.GoToHome()}  className='btn-rounded btn-primary' >
                                    <IonText className='white' >Iniciar sesion</IonText>
                                </IonButton>
                            </div>

                            <div onClick={()=>props.goToRegister()} className='text-center m-t-2'  >
                                <IonText  className='text-color txt-small ' >¿No tienes una cuenta</IonText><IonText className='txt-small '  color="primary"> Registrate aqui</IonText> ?
                            </div>
                              


                        </IonCol>

                    </IonRow>
                </IonGrid>



            </IonContent>

        </IonPage>
    )

}