import React from "react";
import {IonPage,IonContent, IonHeader, IonTitle,IonToolbar,IonButtons,IonBackButton, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonRow, IonCol, IonIcon, IonText, IonList, IonItem} from "@ionic/react";
import { wallet} from 'ionicons/icons';
import CardTransaction from "../components/CardTransaction";
import {FormatValue} from "../utils/methods";

export default function TransactionHistory(props){

  console.log(props);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref='/tabview/myaccount'  />
                    </IonButtons>
                    <IonTitle>Historial de transferencias</IonTitle>
                </IonToolbar>

            </IonHeader>
            <IonContent>
                
                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            Cuenta Nº {props.Account?.Account.accountId}
                        </IonCardTitle>
                        <br/>
                        <IonRow>
                                <IonCol size='2' >
                                    <IonIcon className='ic-wallet' icon={wallet} />
                                </IonCol>
                                <IonCol size='10' >
                                        <IonText className='wallet-label-price' >$ {FormatValue(props.Account?.Account.accountBalance)} COP</IonText>
                                </IonCol>
                            </IonRow>
                    </IonCardHeader>

                </IonCard>
                <div className='transfer-list-container' >

                    <div className='header-list-transfer' >
                         <div onClick={()=>props.setTypeView("SENDED")}  className={ 'header-list-option '+( props.typeView == 'SENDED'? 'header-list-option-selected':"")} >
                                Enviadas
                         </div>
                         <div onClick={()=>props.setTypeView("RECEIVED")} className={ 'header-list-option '+( props.typeView == 'RECEIVED'? 'header-list-option-selected':"")} >
                                Recibidas
                         </div>
                    </div>
                    <IonList>
                        
                            {
                                props.transactionPainted?.map(ele=>
                                    <IonItem
                                    key={ele.transaccionId.toString()}
                                    >
                                    <CardTransaction
                                    accountNumber={ele.Account.accountNumber}
                                    accountNick={ele.accountNick}
                                    amountTransaction={`$  ${props.typeView == 'SENDED'? '-':'+'} ${  (FormatValue(ele.transacionAmount))   } COP `}
                                    />
                                    </IonItem>
                                )
                            }
                              

                        
                    </IonList>

                </div>

            </IonContent>
        </IonPage>
    )

}