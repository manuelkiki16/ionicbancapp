import React from "react";
import {useDispatch,useSelector} from "react-redux";
import {AlertAction,LoadingAction} from "../Redux/actions";
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonTitle,
  IonToolbar,

  IonButtons,
  IonPage,
  IonBackButton,
  IonInput,
  IonSelect,
 IonSelectOption,
 IonButton
  

  } from '@ionic/react';
import { book, build, colorFill, grid,logoVimeo,settings,personCircle,search,options } from 'ionicons/icons';



export default function AddAccount (props){



    let entityes = useSelector( state=>state.Entitis );


    React.useEffect(()=>{
     
    });

    
    return (
        <IonPage>
          
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton  defaultHref='/tabview/myaccount'  />
                    </IonButtons>
                    <IonTitle>Añadir cuenta {props.typeReg == 'SELF'?'propia':"tercero"}</IonTitle>
                </IonToolbar>

            </IonHeader>
            <IonContent>

                <IonCard className='card-form-container' >
                    <IonCardHeader>
                        <IonCardTitle> {props.typeReg == 'SELF'? ' Nueva cuenta':'Registrar cuenta de tercero'  }</IonCardTitle>
                        <IonCardSubtitle>{props.typeReg == 'SELF'? 'Dijite los datos de la nueva cuenta':'Dijite los datos de la cuenta del tercero'}</IonCardSubtitle>
                        
                    </IonCardHeader>
                    <IonCardContent>
                        <IonItem>
                                    <IonLabel color='primary' position="stacked">Numero de cuenta</IonLabel>
                                    <IonInput type="number" onIonChange={evt=>props.setAccountNumber(evt.target.value)} className='ion-input' placeholder='eje:59494853948'  ></IonInput>
                                    { props.validdationsFIelds.accountNumber.vs == true? <label className='label-error' >{props.validdationsFIelds.accountNumber.msg}</label>:<></>}
                        </IonItem>
                        
                        <IonItem>
                                    <IonLabel color='primary' position="stacked">Tipo de cuenta</IonLabel>
                                    <IonSelect onIonChange={evt=>props.setAccountType(evt.target.value)} >
                                       
                                        <IonSelectOption value="AHORRO" >Ahorro</IonSelectOption>
                                        <IonSelectOption value="CORRIENTE" >Corriente</IonSelectOption>
                                    </IonSelect>
                                    { props.validdationsFIelds.accountType.vs == true? <label className='label-error' >{props.validdationsFIelds.accountType.msg}</label>:<></>}
                        </IonItem>
                        <IonItem>
                                    <IonLabel color='primary' position="stacked">Alias de la cuenta</IonLabel>
                                    <IonInput onIonChange={evt=>props.setAccountNick(evt.target.value)} className='ion-input' placeholder='eje: cuenta test'  ></IonInput>
                                    { props.validdationsFIelds.accountNick.vs == true? <label className='label-error' >{props.validdationsFIelds.accountNick.msg}</label>:<></>}
                        </IonItem>
                        <IonItem>
                                    <IonLabel color='primary' position="stacked">Documento de identidad</IonLabel>
                                    <IonInput onIonChange={evt=>props.setAccountDocId(evt.target.value)} className='ion-input' type="number" placeholder='cuenta test'  ></IonInput>
                                    { props.validdationsFIelds.accountDocId.vs == true? <label className='label-error' >{props.validdationsFIelds.accountDocId.msg}</label>:<></>}
                        </IonItem>
                      
                        <IonItem>
                                    <IonLabel color='primary' position="stacked">Entidad bancaria</IonLabel>
                                    <IonSelect onIonChange={evt=>props.setAccountEntitiId(evt.target.value)}  >
                                       {
                                           entityes.map( ele=>
                                            <IonSelectOption key={ele.entitiId.toString()} value={ele.entitiId} >{ele.entitiCode} - {ele.entitiName}</IonSelectOption>
                                            )
                                       }
                                      
                                     
                                    </IonSelect>
                                    { props.validdationsFIelds.accountEntitiId.vs == true? <label className='label-error' >{props.validdationsFIelds.accountEntitiId.msg}</label>:<></>}
                        </IonItem>
                        <IonButton onClick={props.OnRegisterAccount} className='m-t-2'  size="default" expand="block" fill={"solid"} color='primary'  >
                            Crear cuenta
                        </IonButton>
                    </IonCardContent>
                    
                </IonCard>

            

            </IonContent>
        </IonPage>

    )

}