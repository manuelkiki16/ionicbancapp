import React from "react";
import {IonPage,IonHeader,IonContent,IonSpinner,IonAlert,IonToolbar,IonButton,IonGrid,IonRow,IonCol,IonList,IonItem,IonLabel,IonInput,IonImg,IonText} from "@ionic/react";
import IcLogo from "../assets/images/ic_logo.svg";
import Service from "../service/AxiosInstance";
import { useHistory } from "react-router";

export default function SuccessReg(props){

let history = useHistory();

return (
<IonPage>
    <IonContent>

        <div className='initContainer' >
           <label className='congratulation-title' >Felicitacion</label> 
           <label className='congratulation-description' >Has completado el proceso de registro  </label> 
           <IonButton onClick={()=>history.push("/Login")} color='primary' >Ir al inicio de sesion</IonButton>
        </div>

    </IonContent>
</IonPage>)

}