import React from "react";
import {IonPage,IonHeader,IonContent,IonSpinner,IonAlert,IonToolbar,IonButton,IonGrid,IonRow,IonCol,IonList,IonItem,IonLabel,IonInput,IonImg,IonText} from "@ionic/react";
import IcLogo from "../assets/images/ic_logo.svg";
import Service from "../service/AxiosInstance";


export default function InitView(props){

    
return (
    <IonPage>
        
        <IonContent>
               <div className='initContainer' >

                   <img src={IcLogo} className='init-logo'   />
                   <IonText className='init-title' >BankApp</IonText>
                   <IonSpinner   />
               
               </div> 
        </IonContent>
    </IonPage>
);


}