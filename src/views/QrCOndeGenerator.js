import React, { useEffect } from "react";
import {IonPage,
    IonCard,
    IonCardHeader,
    IonCardContent,
    IonContent,
    IonRow,
    IonCol,
    IonHeader,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButtons,
    IonCardTitle,
    IonText} from "@ionic/react";
import QRCode from "qrcode.react";
import {useDispatch,useSelector} from "react-redux";


export default function (props) {

    let MyAccountSelected = useSelector(state=>state.MyAccountSelected);


    useEffect(()=>{



    },[]);



    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/" />
                    </IonButtons>
                    <IonTitle>Codigo QR de de cuenta</IonTitle>
                </IonToolbar>

            </IonHeader>
            <IonContent>

            <IonCard>
                <IonCardHeader>
                    <IonCardTitle>Codigo QR</IonCardTitle>
                </IonCardHeader>
                <IonCardContent>

                <IonRow>
                    <IonCol size="12" >
                        <QRCode renderAs="svg" className='qr-image'  value={`${MyAccountSelected.accountInscriptionId}-${MyAccountSelected.accountInscriptionDocId}-${MyAccountSelected.Account.accountNumber}`} />
                    </IonCol>
                    <IonCol size="12" >
                       <IonText><strong>CUENTA ALIAS:</strong> {MyAccountSelected?.accountInscriptionNickName}</IonText>
                    </IonCol>
                    <IonCol size="12" >
                       <IonText><strong>CUENTA N°: </strong>{MyAccountSelected?.Account.accountNumber}</IonText>
                    </IonCol>
                    <IonCol size="12" >
                       <IonText><strong>TIPO DE CUENTA°: </strong>{MyAccountSelected?.Account.accountType}</IonText>
                    </IonCol>
                   
                </IonRow>


                </IonCardContent>
            </IonCard>

            </IonContent>
        </IonPage>
    )

}