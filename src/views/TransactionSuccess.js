import React,{useEffect} from "react";
import {IonPage,IonContent, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonText, IonButton} from "@ionic/react";
import {useDispatch,useSelector} from "react-redux";
import {useParams,useHistory} from "react-router-dom";
import {ClearTransactionData} from "../Redux/actions";
import {FormatValue} from "../utils/methods";

export default function TransactionSuccess(props){

    const dispatch = useDispatch();
    let history = useHistory();
    const transactionData = useSelector(state=>state.transactionDetail);
    const AccountOrigin = useSelector(state=>state.MyAccountSelected);
    const AccountDesty = useSelector(state=>state.OtherAccountSelected);


    function GotoHome(){

        dispatch(ClearTransactionData({}));
        history.replace(`/tabview/myaccount`);


    }

return (
    <IonPage>
        <IonContent>

               <div className='contained-centered' >
                <IonCard>
                        <IonCardHeader>
                            <IonCardTitle className='text-center' >
                                Transferencia exitosa
                            </IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                        <div  >
                                <IonText><strong>Transferencia Id: </strong> {transactionData?.transaccionId}</IonText>
                                <br/>
                                <IonText><strong>Cuenta de origen: </strong> {AccountOrigin?.Account?.accountNumber}</IonText>
                                <br/>
                                <IonText><strong>Cuenta de destino </strong> {AccountDesty?.Account?.accountNumber}</IonText>
                                <br/>
                                <IonText><strong>Cantidad: </strong> {FormatValue(transactionData?.transacionAmount)}</IonText>
                                <br/>
                                <IonText><strong>Detalle: </strong>{transactionData?.transactionDescription}</IonText>
                        </div>
                        

                        </IonCardContent>
                    </IonCard>
                    <IonButton onClick={()=>GotoHome()} expand="block" className='success text-white' >
                        Ir al inicio
                    </IonButton>
               </div>

        </IonContent>
    </IonPage>
)

}