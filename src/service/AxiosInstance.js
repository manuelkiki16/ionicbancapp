import {baseUri,sesionStorageId} from "../utils/contants";
import axios from "axios";
import {useDispatch} from "react-redux";
import {AlertAction} from "../Redux/actions";

const BankService =  axios.create({
    baseURL: baseUri,
  });

  BankService.interceptors.request.use(function (req) {
    // Do something before request is sent
      let tokenAuth = localStorage.getItem(sesionStorageId);
      req.headers.authorization ="Bearer "+(tokenAuth?tokenAuth:'');
    return req;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error.status);
  });

  


export default BankService;