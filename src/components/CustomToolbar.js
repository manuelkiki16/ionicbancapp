import React from "react";
import {IonText,IonBackButton,IonGrid,IonRow,IonCol} from "@ionic/react";

export default function (props){

return (
    <div className='custom-toolbar-container' >
        <IonGrid>
            <IonRow>
                <IonCol size='2' >
                <IonBackButton />
                </IonCol>
                <IonCol className='row-text-center' size='8' >
                <IonText>Custom title</IonText>
                </IonCol>
                <IonCol size='2' >
                    
                </IonCol>
            </IonRow>
        </IonGrid>
          
    </div>
)

}