import React, { useEffect } from "react";
import { IonActionSheet, IonContent, IonButton } from '@ionic/react';
import PropTypes from "prop-types";
import { trash,swapHorizontal,qrCode,close,reader } from "ionicons/icons";



  const AccountActionComponent = (props)=>{

    useEffect(()=>{


    },[]);


    return (
        <IonActionSheet
        isOpen={props.visible}
        onDidDismiss={props.CancelSheet}
        cssClass='my-custom-class'
        buttons={[
        {
          text: 'Tranferencia a cuenta propia',
          icon: swapHorizontal,
          handler: () => props.onSelectOption("T-P")
        },
        {
            text: 'Tranferencia a cuenta de terceros',
            icon: swapHorizontal,
            handler: () => props.onSelectOption("T-T")
        },
       
        {
            text: 'Historial de transferencias',
            icon: reader,
            handler: () => props.onSelectOption("H-T")
        },
        {
            text: 'Generar codigo QR',
            icon: qrCode,
            handler: () => props.onSelectOption("QR-G")
        },
        {
          text: 'Transferir por codigo QR',
          icon: qrCode,
          handler: () => props.onSelectOption("T-C-G")
      },
        {
          text: 'Cancelar',
          icon: close,
       
          handler: () => props.onCloseOpe()
        }]}
      >
      </IonActionSheet>
    )
  }

  AccountActionComponent.propTypes = {
      visible:PropTypes.bool.isRequired,
      onSelectOption:PropTypes.func.isRequired,
      onCloseOpe:PropTypes.func.isRequired,
      CancelSheet:PropTypes.func.isRequired
  }

export default AccountActionComponent;