import React from "react";
import {IonModal,IonButton,IonCard, IonTitle, IonContent,IonHeader,IonToolbar, IonRow, IonCol, IonText} from "@ionic/react";
import PropTypes from "prop-types";
import AccountCardRo from "../components/AccountCardReadOnly";

const  ModalAccountList = (props)=>{


    return (
    
        <IonModal onDidDismiss={()=>props.changeVisibility(false)} isOpen={props.isVisible} >
     
            <IonHeader>
                <IonToolbar>
                 
                <IonTitle>Cuentas vinculadas</IonTitle>
                </IonToolbar>

            </IonHeader>
        <IonContent>

            <IonRow>
                <IonCol size="12" >

                    {
                        props.accountList.length  == 0?
                        <div className='not-account-vinclued' >
                             <IonTitle  >Lista vacia</IonTitle>
                             <br/>
                            <IonText>Todavia no tienes cuentas vinculadas</IonText>
                        </div>:
                          props.accountList.map( (ele)=>
                            <AccountCardRo 
                            key={ele.accountInscriptionId.toString()}
                            typeAccount={ele.Account.accountType}
                            accountNumber={ele.Account.accountNumber}
                            accountId={ele.Account.accountId}
                            accountNick={ele.accountInscriptionNickName}
                            onClickCard={()=>props.selectAccount(ele)}
                            />
                            )
                    }

                    

                </IonCol>
                <IonCol size="12" >
                        <IonButton onClick={()=>props.changeVisibility(false)} expand="block" color="danger" >Cerrar ventana</IonButton>
                </IonCol>   
            </IonRow>            

        </IonContent>
      </IonModal>
    )
    
}


ModalAccountList.propTypes = {
    isVisible:PropTypes.bool.isRequired,
    changeVisibility:PropTypes.func.isRequired,
    selectAccount:PropTypes.func.isRequired,
    accountList:PropTypes.array.isRequired
};




export default ModalAccountList



