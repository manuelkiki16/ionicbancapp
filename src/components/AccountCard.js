import React, { useEffect } from "react";
import { IonCard, IonCardHeader, IonCardSubtitle,IonCardContent, IonCardTitle, IonGrid, IonRow,IonCol, IonText,IonButton, IonIcon } from "@ionic/react";
import  PropTypes from "prop-types";
import {FormatValue} from "../utils/methods";
import { ellipsisVertical} from 'ionicons/icons';

const AccountCard = (props)=>{


    


    return (
        <IonCol size='12' >
            <IonCard >

                <IonCardHeader>
                    <IonCardSubtitle>{props.typeAccount == 'AHORRO' ? 'Cuenta de ahorro' : "Cuenta corriente"}</IonCardSubtitle>
                    <IonCardTitle>Cuenta Nº {props.accountNumber}</IonCardTitle>

                </IonCardHeader>
                <IonCardContent>
                    <IonGrid>
                        <IonRow>
                            <IonCol size='10' >
                               <IonText  >Disponible $ {FormatValue(props.accountBalance)} COP</IonText>
                            </IonCol>
                            <IonCol size='2' >
                              <IonCard onClick={()=>props.OnOpenSheet(props.index)}  className='btn-op-content'   >
                             
                                <IonIcon icon={ellipsisVertical} />
                            
                              </IonCard>
                            </IonCol>

                        </IonRow>
                    </IonGrid>
                </IonCardContent>
            </IonCard>
        </IonCol>
    )
    
    }
 
AccountCard.propTypes = {
    typeAccount:PropTypes.string.isRequired,
    accountNumber:PropTypes.string.isRequired,
    accountId:PropTypes.number.isRequired,
    accountBalance:PropTypes.number,

}  
export default AccountCard;