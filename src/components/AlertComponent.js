import React from "react";
import {
    IonAlert,
  } from '@ionic/react';
import PropTypes from 'prop-types';

const AlertComponent = (props)=>{

    return (<IonAlert
        isOpen={props.visible}
        onDidDismiss={props.callback?props.callback:()=>{}}
        //cssClass='my-custom-class'
        header={ props.title?props.title:'Mensaje'}
        //subHeader={'Subtitle'}
        message={props.message}
        buttons={[
          {
            text:"OK",
            handler:props.closeCallback
          }
        ]}
      />)
}

AlertComponent.propTypes ={
title:PropTypes.string,    
visible:PropTypes.bool.isRequired,
message:PropTypes.string.isRequired,
callback:PropTypes.func,
closeCallback:PropTypes.func.isRequired,
};


export default AlertComponent;