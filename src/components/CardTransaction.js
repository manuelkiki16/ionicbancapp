import React from "react";
import {IonCard,IonCardContent,IonCardTitle,IonCardHeader,IonCardSubtitle, IonText} from "@ionic/react";
import PropTypes from "prop-types";

const  CardTransaction = (props)=>{


    return (
        <IonCard class='list-card' >
            <IonCardHeader>
                <IonCardSubtitle>Cuenta nº {props.accountNumber} </IonCardSubtitle>
               
            </IonCardHeader>
            <IonCardContent>
            <IonCardSubtitle>Alias: {props.accountNick}</IonCardSubtitle>
            <IonText>Cantidad: {props.amountTransaction}</IonText>
            </IonCardContent>
        </IonCard>
    )

}
CardTransaction.propTypes = {
    accountNumber:PropTypes.any.isRequired,
    accountNick:PropTypes.any.isRequired,
    amountTransaction:PropTypes.any.isRequired
}

export default CardTransaction;