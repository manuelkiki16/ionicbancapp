import React, { useEffect } from "react";
import { IonCard, IonCardHeader, IonCardSubtitle,IonCardContent, IonCardTitle, IonGrid, IonRow,IonCol, IonText,IonButton, IonIcon } from "@ionic/react";
import  PropTypes from "prop-types";
import {FormatValue} from "../utils/methods";
import { ellipsisVertical} from 'ionicons/icons';


const AccountCardReadOnly = (props)=>{


    


    return (
        <IonCol onClick={props.onClickCard}  size='12' >
            <IonCard >

                <IonCardHeader>
                    <IonCardSubtitle>{props.typeAccount == 'AHORRO' ? 'Cuenta de ahorro' : "Cuenta corriente"}</IonCardSubtitle>
                    <IonCardTitle>Cuenta Nº {props.accountNumber}</IonCardTitle>

                </IonCardHeader>
                <IonCardContent>
                    <IonGrid>
                        <IonRow>
                            <IonCol size='12' >
                               <IonText  ><strong>Alias: </strong>{props.accountNick}</IonText>
                            </IonCol>
                           

                        </IonRow>
                    </IonGrid>
                </IonCardContent>
            </IonCard>
        </IonCol>
    )
    
    }
 
AccountCardReadOnly.propTypes = {
    typeAccount:PropTypes.string.isRequired,
    accountNumber:PropTypes.any.isRequired,
    accountId:PropTypes.any.isRequired,
    accountNick:PropTypes.string,
    onClickCard:PropTypes.func.isRequired

}    

export default AccountCardReadOnly;

  

