import Identifiers from "./Identifiers";


export const AlertAction = (payload)=>{

return {
    type:Identifiers.SET_ALERT_GLOBAL,
    payload,
}

}
export const SetTransactionDetail = (payload)=>{
    return {
        type:Identifiers.SET_TRANSACTION_DETAIL,
        payload,
    }
}
export const ClearTransactionData = (payload)=>{
    return {
        type:Identifiers.CLEAR_TRANSACTION_DATA,
        payload,
    }
}
export const SetSesionStatus = (payload)=>{

    return {
        type:Identifiers.SET_SESION_STATUS,
        payload,
    }
    
}

export const SetMyAccountSelected =(payload)=>{

    return {
        type:Identifiers.SET_MY_ACCOUNT_SELECTED,
        payload
    }

}
export const SetOtherAccountSelected =(payload)=>{

    return {
        type:Identifiers.SET_OTHER_ACCOUNT_SELECTED,
        payload
    }

}
export const SetSelfAccounts = (payload)=>{

    return {
        type:Identifiers.SET_SELF_ACCOUNT_LIST,
        payload
    }
    
    }
export const LoadingAction = (payload)=>{

    return {
        type:Identifiers.SET_LOADING_GLOBAL,
        payload
    }
    
    }
export const SetEntities = (payload)=>{

    return {
        type:Identifiers.SET_ENTITIES,
        payload
    }
    
}

export const ResetState = ()=>{

    return {
        type:Identifiers.RESET_STATE,
        payload:{}
    }
    
}