import Identifiers from "./Identifiers";


let InitialState = {
SesionStatus:false,    
AlertGLobal:{
msg:"",
visible:false,
back:false
}, 
LoadingGLobal:{
msg:"",
visible:false
},
MyacountList:[],
transactionDetail:{},
Entitis:[],
OtherAccountLIst:[],
MyAccountSelected:{},
OtherAccountSelected:{},
TransactionsSended:[],
TransactionReceived:[],
TransactionDetail:{}
};



export  function StoreApp(state=InitialState,action){

    let newState = {...state};

switch (action.type) {
   
    case Identifiers.SET_SELF_ACCOUNT_LIST:
        newState.MyacountList =  action.payload;
        return newState;
    break;
    case Identifiers.SET_TRANSACTION_DETAIL:
        newState.transactionDetail =  action.payload;
        return newState;
    break;
    case Identifiers.CLEAR_TRANSACTION_DATA:
        newState.transactionDetail =  {};
        newState.OtherAccountSelected =  {};
        return newState;
    break;
    case Identifiers.SET_SESION_STATUS:
        newState.SesionStatus =  action.payload;
        return newState;
    break;
    case Identifiers.SET_SELF_ACCOUNT_LIST:
        newState.MyacountList =  action.payload;
        return newState;
    break;
    case Identifiers.SET_MY_ACCOUNT_SELECTED:
        newState.MyAccountSelected =  action.payload;
        return newState;
    break;
    case Identifiers.SET_OTHER_ACCOUNT_SELECTED:
        newState.OtherAccountSelected =  action.payload;
        return newState;
    break;
    case Identifiers.SET_LOADING_GLOBAL:
        newState.LoadingGLobal = action.payload;
        return newState;
    break;

    case Identifiers.SET_ALERT_GLOBAL:
        newState.AlertGLobal = action.payload;
        return newState;
    break;  
    
    case Identifiers.SET_ENTITIES:
        newState.Entitis = action.payload;
        return newState;
    break;    
    
    case Identifiers.SET_SELF_ACCOUNT_LIST:
        return {...state,MyacountList:action.payload};
    break;    

    default:
        return state;
        break;
}
return newState;



};