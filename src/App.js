import React,{useEffect,useState} from "react";
import { Redirect, Route, useHistory } from 'react-router-dom';
import {
  IonApp,
  IonRouterOutlet,

  IonAlert,
  IonLoading
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Routes from "./routes/init";
import {useDispatch,useSelector} from "react-redux";
import {SetEntities, LoadingAction} from "./Redux/actions";


/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import "./theme/custom.css";

const App = () => {

  let AlertState = useSelector(state=>state.AlertGLobal);
  let LoadingState = useSelector(state=>state.LoadingGLobal);
  let dispatch = useDispatch();
  let history = useHistory();
  
  useEffect(()=>{
    console.log("loading",LoadingState,AlertState);
    console.log(history);
  },[LoadingState,AlertState]);
  
  return(

  <IonApp>



<IonLoading
  isOpen={LoadingState.visible}
  message={LoadingState.msg?LoadingState.msg:" Cargando..."}

/>
      

    <IonReactRouter>
    <IonRouterOutlet>

  

{
  Routes.map( (ele)=>
    <Route key={ele.nameroute} exact={ele.exact} path={ele.path} render={props=>
      <ele.component {...props}   routename={ele.nameroute} subroutes={ele.subroutes}  />
    } >
      
    </Route>
  )
}


</IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
)};

export default App;
