# Banco App

Aplicacion para banco test

### Stack Utilizado
[ionic](https://ionicframework.com/)  
[apache cordova](https://cordova.apache.org/)  
[React js](https://es.reactjs.org/docs/getting-started.html)  
[Patron MVC](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador)  



### Pre-requisitos 📋

_Se necesita tener node.js,Ionic,cordova y las variables de entorno para android
Recuerde primero ejecutar el api antes de iniciar la aplicacion





## Comenzando 🚀



**PASO 1 ** Clone el proyecto en gitlab con el siguiente comando

```
git  clone https://gitlab.com/manuelkiki16/ionicbancapp
```
**PASO 2 ** Ubiquese en el directorio raiz y ejecute

```
npm install

```


**PASO 3** Dirijase a la carpeta raiz del proyecto y ejecute el siguiente comando

```
ionic serve

```





Mira [IONIC DEPLOY](https://ionicframework.com/docs/react/your-first-app/6-deploying-mobile) para conocer como desplegar el proyecto.



### Eso es todo de momento

